package com.example.achievements;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AchievementsApplication {

	//	https://getbootstrap.com/docs/4.0/examples/

	public static void main(String[] args) {
		SpringApplication.run(AchievementsApplication.class, args);
	}

}
