package com.example.achievements.controller;

import com.example.achievements.domain.repository.AchievementsRepository;
import com.example.achievements.dto.AchievementDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AchievementsController {

    private final AchievementsRepository repository;

    public AchievementsController(AchievementsRepository repository) {
        this.repository = repository;
    }

    @RequestMapping("/")
    public String list() {
        return "achievements";
    }

    public void addAchievement(AchievementDto achievementDto) {

    }

}
