package com.example.achievements.domain.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class AchievementsUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false, unique = true)
    private String username;

    private String password;

    private String role;

    private boolean enabled;
}
