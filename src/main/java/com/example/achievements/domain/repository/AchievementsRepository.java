package com.example.achievements.domain.repository;

import com.example.achievements.domain.entity.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AchievementsRepository extends JpaRepository<Achievement, Long> {

}
