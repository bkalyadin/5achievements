package com.example.achievements.domain.repository;

import com.example.achievements.domain.entity.AchievementsUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<AchievementsUser, Long> {
    AchievementsUser findByUsername(String username);
}
